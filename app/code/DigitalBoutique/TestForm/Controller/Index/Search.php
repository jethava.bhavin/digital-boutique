<?php
/**
 * @category  Digital Boutique Custom Form
 * @package   DigitalBoutique_TestForm
 * @copyright Copyright (c) 2022 Bhavin
 * @author    Bhavin
 */
namespace DigitalBoutique\TestForm\Controller\Index;

use DigitalBoutique\TestForm\Model\SearchLogFactory;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Store\Model\StoreManagerInterface;

class Search extends \Magento\Framework\App\Action\Action {
	/**
	 * @var \DigitalBoutique\TestForm\Model\SearchLogFactory
	 */
	protected $_searchLog = false;
	/**
	 * @var _storeManager
	 */
	protected $_storeManager;
	/**
	 * @var \Magento\Customer\Model\Session
	 */
	protected $_customerSession;
	/**
	 * @var Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
	 */
	protected $_remoteAddress;

	/**
	 * @param Context $context
	 * @param CustomerSession $_customerSession
	 * @param SearchLogFactory $searchLog
	 * @param StoreManagerInterface $storeManager
	 * @param RemoteAddress $remoteAddress
	 */
	public function __construct(
		Context $context,
		CustomerSession $_customerSession,
		SearchLogFactory $searchLogFactory,
		StoreManagerInterface $storeManager,
		RemoteAddress $remoteAddress
	) {
		$this->_searchLog = $searchLogFactory;
		$this->_customerSession = $_customerSession;
		$this->_storeManager = $storeManager;
		$this->_remoteAddress = $remoteAddress;
		parent::__construct($context);
	}

	/**
	 * @return mixed
	 */
	public function execute() {
		$q = $this->getRequest()->getParam("skus");
		if ($q) {
			$user_id = 0;
			if ($customer = $this->_customerSession->getCustomer()) {
				$user_id = $customer->getId();
			}
			$ip = $this->_remoteAddress->getRemoteAddress();
			$store_id = $this->_storeManager->getStore()->getId();

			$searchLog = $this->_searchLog->create();

			$searchLog
				->setUserId($user_id)
				->setStoreId($store_id)
				->setIp($ip)
				->setSearchQuery($q)
				->save();
		}
		$this->_eventManager->dispatch(
			'digital_boutique_form',
			['request' => $this->getRequest()]
		);
		return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
	}
}