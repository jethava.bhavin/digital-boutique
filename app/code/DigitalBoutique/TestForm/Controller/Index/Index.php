<?php
/**
 * @category  Digital Boutique Custom Form
 * @package   DigitalBoutique_TestForm
 * @copyright Copyright (c) 2022 Bhavin
 * @author    Bhavin
 */
namespace DigitalBoutique\TestForm\Controller\Index;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action implements HttpGetActionInterface {
	/**
	 * Show Contact Us page
	 *
	 * @return \Magento\Framework\Controller\ResultInterface
	 */
	public function execute() {
		return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
	}
}
