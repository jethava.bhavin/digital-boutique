<?php
declare (strict_types = 1);
/**
 * @category  Digital Boutique Custom Form
 * @package   DigitalBoutique_TestForm
 * @copyright Copyright (c) 2022 Bhavin
 * @author    Bhavin
 */
namespace DigitalBoutique\TestForm\Controller;

use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;

/**
 * Class Router
 */
class Router implements RouterInterface {
	/**
	 * @var ActionFactory
	 */
	private $actionFactory;
	/**
	 * Router constructor.
	 *
	 * @param ActionFactory $actionFactory
	 */
	public function __construct(
		ActionFactory $actionFactory
	) {
		$this->actionFactory = $actionFactory;
	}
	/**
	 * @param RequestInterface $request
	 * @return ActionInterface|null
	 */
	public function match(RequestInterface $request):  ? ActionInterface {

		if ($request->getFrontName() == 'digital-boutique') {
			$request->setModuleName('digitalboutique');
			$request->setControllerName('index');
			return $this->actionFactory->create(Forward::class, ['request' => $request]);
		}

		return null;
	}
}