<?php
/**
 * @category  Digital Boutique Custom Form
 * @package   DigitalBoutique_TestForm
 * @copyright Copyright (c) 2022 Bhavin
 * @author    Bhavin
 */
namespace DigitalBoutique\TestForm\Block;

class CustomForm extends \Magento\Framework\View\Element\Template {
	/**
	 * getFormUrl
	 *
	 * return string url
	 */
	public function getFormUrl() {
		return $this->getUrl("digital-boutique/m2-test/search");
	}
}