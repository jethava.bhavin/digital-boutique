<?php
/**
 * @category  Digital Boutique Custom Form
 * @package   DigitalBoutique_TestForm
 * @copyright Copyright (c) 2022 Bhavin
 * @author    Bhavin
 */
namespace DigitalBoutique\TestForm\Block;

class SearchResult extends \Magento\CatalogWidget\Block\Product\ProductsList {
	/**
	 * Prepare product collection
	 *
	 * @return Collection
	 */
	public function createCollection() {
		if(!$this->getProductCollection()){
			/** @var $collection Collection */
			$collection = $this->productCollectionFactory->create();

			if ($this->getData('store_id') !== null) {
				$collection->setStoreId($this->getData('store_id'));
			}

			$collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

			$collection = $this->_addProductAttributesAndPrices($collection)
				->addStoreFilter()
				->addAttributeToSort('created_at', 'desc')
				->setPageSize($this->getPageSize())
				->setCurPage($this->getRequest()->getParam($this->getData('page_var_name'), 1));

			/**
			 * Prevent retrieval of duplicate records. This may occur when multiselect product attribute matches
			 * several allowed values from condition simultaneously
			 */
			$collection->distinct(true);
			$skus = $this->getRequest()->getParam("skus");
			if ($skus) {
				$collection->addAttributeToFilter("sku", ["like" => "%" . $skus . "%"]);
			} else {
				$collection->addFieldToFilter("entity_id", 0);
			}
			$this->setProductCollection($collection);
		}
		return $this->getProductCollection();
	}
}