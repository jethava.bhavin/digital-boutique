<?php
/**
 * @category  Digital Boutique Custom Form
 * @package   DigitalBoutique_TestForm
 * @copyright Copyright (c) 2022 Bhavin
 * @author    Bhavin
 */

namespace DigitalBoutique\TestForm\Model\ResourceModel\TestFormCustom;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
	/**
	 * ID Field Name
	 *
	 * @var string
	 */
	protected $_idFieldName = 'id';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct() {
		$this->_init('DigitalBoutique\TestForm\Model\SearchLog', 'DigitalBoutique\TestForm\Model\ResourceModel\SearchLog');
	}

	/**
	 * Get SQL for get record count.
	 * Extra GROUP BY strip added.
	 *
	 * @return \Magento\Framework\DB\Select
	 */
	public function getSelectCountSql() {
		$countSelect = parent::getSelectCountSql();

		$countSelect->reset(\Zend_Db_Select::GROUP);

		return $countSelect;
	}
	/**
	 * @param string $valueField
	 * @param string $labelField
	 * @param array $additional
	 * @return array
	 */
	protected function _toOptionArray($valueField = 'id', $labelField = 'name', $additional = []) {
		return parent::_toOptionArray($valueField, $labelField, $additional);
	}

}