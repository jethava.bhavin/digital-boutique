<?php
/**
 * @category  Digital Boutique Custom Form
 * @package   DigitalBoutique_TestForm
 * @copyright Copyright (c) 2022 Bhavin
 * @author    Bhavin
 */

namespace DigitalBoutique\TestForm\Model;

class SearchLog extends \Magento\Framework\Model\AbstractModel {
	/**
	 * Model Initialization
	 *
	 * @return void
	 */
	protected function _construct() {
		parent::_construct();

		$this->_init('DigitalBoutique\TestForm\Model\ResourceModel\SearchLog');
	}

	/**
	 * @return mixed
	 */
	public function getDefaultValues() {
		$values = [];

		return $values;
	}
}
